import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const ApiRoutes = {
  login: 'login',
  signup: 'signup',
  users: 'users'
};

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor() { }

}
