const express = require('express');
const router = express.Router();

const clienteCtrl = require('../Controllers/cliente.controller');

router.get('/', clienteCtrl.getClientes);
router.post('/', clienteCtrl.createCliente);
router.get('/:id', clienteCtrl.getCliente);
router.put('/:id', clienteCtrl.editCliente);
router.delete('/:id', clienteCtrl.deleteCliente);

module.exports = router;