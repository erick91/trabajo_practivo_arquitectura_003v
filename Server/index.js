const express = require('express');
const morgan = require('morgan');
const app = express();

const { mongoose } = require('./database');

app.set('port', process.env.PORT || 3000);

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/cliente',require('./Routes/cliente.routes'));

app.listen(app.get('port'), () => {
    console.log('Servidor en puerto', app.get('port'));
});
