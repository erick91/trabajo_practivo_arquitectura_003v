const cliente = require('../Models/cliente');
const clienteCtrl = {};

//busca todos los clientes
clienteCtrl.getClientes =  async (req, res) => {
    const clientes = await cliente.find();
    res.json(clientes);
}

//busca un usuario por id
clienteCtrl.getCliente = async (req, res) =>  {
    const cliente = new Cliente(req.body);
    await cliente.save();
    res.json({'status': 'empleado recibido' });
}

clienteCtrl.createCliente =  async (req, res) => {
    const cliente = await cliente.findById(req.params.id);
    res.json(cliente);
}

clienteCtrl.editCliente = async (req, res) => {
    const {id} = req.params;
    const cliente =  {
        nombre: req.params.nombre,
        apellidos: req.params.apellidos,
        correo: req.params.correo,
        telefono: req.params.telefono,
        direccion: req.params.direccion,
        password: req.params.password
    };
    await cliente.finByIdAndUpdate(id, {$set: cliente});
    res.json({status: 'Cliente actualizado'});
}

clienteCtrl.deleteCliente = async (req, res) => {
    await cliente.findByIdAndRemove(req.params.id);
    res.json({status: 'Cliente eliminado'});
}

module.exports = clienteCtrl;